"""
FreePBX Bulk Handler uses CSV files for importation.
This package is designed to assist in formatting the CSV file.
"""

from .Extension import *


def __authorship__():
    """
***********************************************************************************************************************
This python module was created by Dax Mickelson along with LOTs of help from my friend and colleague Dmitry Figol.
***********************************************************************************************************************
    """

__authorship__()